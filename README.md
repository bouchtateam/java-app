##### build the project

### nginx.conf 


    http{

    proxy_send_timeout 120;

    proxy_read_timeout 300;

    proxy_buffering    off;

    proxy_request_buffering off;

    keepalive_timeout  5 5;

    tcp_nodelay        on;

    server {

        listen   *:80;

        server_name  repo.example.com;

        # allow large uploads of files - refer to nginx documentation
        client_max_body_size 1G;

        # optimize downloading files larger than 1G - refer to nginx doc before adjusting
        #proxy_max_temp_file_size 2G;

        location / {

            proxy_pass http://localhost:8081/;

            proxy_set_header Host $host;

            proxy_set_header X-Real-IP $remote_addr;

            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

        }

    }

  }

#### 2- gradle configuration :

# vim   gradle.properties

 repoUser=[your nexus user]

 repoPassword=[your nexus password]

 nexusURL=https://domain.com
 
# vim gradle.settings

 rootProject.name = ‘java-app’
 
# vim build.gradle

  version = ‘0.0.1’

  group = ‘ro.trc.common’

  sourceCompatibility = ‘1.8’

  apply plugin: ‘maven-publish’

  publishing {

  publications {

  maven(MavenPublication) {

  artifact(“build/libs/java-app-$version”+”.jar”) {

  extension ‘jar’

  }}}

  repositories {

  maven {

  name ‘nexus’

  url“$nexusURL/repository/maven-releases/"

  credentials {

  username project.repoUser

  password project.repoPassword

  }}}}


### command must be executed to publish the package to nexus

   cd  java-app

   gradle build
 
   gradle publish




    ./gradlew build

##### build Docker image called java-app. Execute from root

    docker build -t java-app .
    
##### push image to repo 

    docker tag java-app demo-app:java-1.0
    

### Changes
[23.Aug.2021]

Gradle wrapper version upgraded from version 6.x to 7.0 
        
###### This will change the version in wrapper.settings

     ./gradlew wrapper --gradle-version 7.0

###### This will update the complete wrapper and download version 7.0 jar

     ./gradlew wrapper --gradle-version 7.0

In build.gradle file, replace:
- compile with implementation 
- testCompile with testImplementation

Because, version 7.0 removed compile and testCompile configurations.
Source: https://docs.gradle.org/current/userguide/upgrading_version_6.html#sec:configuration_removal
